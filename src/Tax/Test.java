package Tax;

import java.util.ArrayList;
import java.util.Collections;

import comparable.Person;
import comparable.Product;
import comparator.Company;

public class Test {

	public static void main(String[] args) {
		ArrayList<Taxable> list = new ArrayList<Taxable>();
		list.add((Taxable) new Person("Nun",200000));
		list.add((Taxable) new Product("House",70000));
		list.add((Taxable) new Company("Big C",500000, 30000));
		
		Collections.sort(list, new TaxCalculator());
		for(Taxable l : list){
			System.out.println(l);
		}
		
		

	}

}
