package comparable;

import Tax.Taxable;

public class Product implements Measurable,Comparable<Product>,Taxable{
	private String name;
	private int price;
	
	public Product(String name, int price){
		this.name = name;
		this.price = price;
	}
	public int getPrice(){
		return price;
	}

	@Override
	public double getMeasure() {
		return price;
	}

	@Override
	public int compareTo(Product otherObject) {
		Product other = (Product) otherObject;
	if(price < other.price){return -1;}
	if(price > other.price){return 1;}
	return 0;
	}
	

	@Override
	public double getTax() {
		double tax;
		tax = (price*7)/100;
		return tax;
	}

	public String toString() {
		return "Name : "+this.name+", Price = "+this.price;
	}
	

}
