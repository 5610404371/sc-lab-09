package comparable;
import java.util.ArrayList;
import java.util.Collections;


public class Test {

	public static void main(String[] args) {
		ArrayList<Person> person = new ArrayList<Person>();
		person.add(new Person("Prow", 30000));
		person.add(new Person("Prim", 20000));
		person.add(new Person("Praew", 50000));

		Collections.sort(person);
		System.out.println("----------Person----------");
		for(Person p : person){
			System.out.println(p);
		}
		
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Car", 200000));
		products.add(new Product("House", 1000000));
		products.add(new Product("Condo", 700000));
		
		Collections.sort(products);
		System.out.println("----------Product----------");
		for(Product pd : products){
			System.out.println(pd);
		}

	}

}
