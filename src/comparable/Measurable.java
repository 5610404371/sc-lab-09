package comparable;

public interface Measurable {
	public double getMeasure(); 
}
