package comparable;

import Tax.Taxable;

public class Person implements Measurable,Comparable<Person>,Taxable {
	private String name;
	private int income;

	public Person(String name, int income) {
		this.name = name;
		this.income = income;
	}
	public int getIncome(){
		return income;
	}

	@Override
	public double getMeasure() {
		return income;
	}

	@Override
	public int compareTo(Person otherObject) {
		Person other = (Person) otherObject;
		if(income < other.income){return -1;}
		if(income > other.income){return 1;}
		return 0;
	}
	
	public String toString() {
		return "Name : "+this.name+", income = "+this.income;
	}
	@Override
	public double getTax() {
		double tax = 0;
		if (income <= 300000){
			tax = (income*5)/100;
		}else if(income > 300000){
			tax = income - 300000; 
			tax = (income*10)/100 + 15000;
		}	
		return tax;
	}

}
