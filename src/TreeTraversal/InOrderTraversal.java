package TreeTraversal;

import java.util.ArrayList;
import java.util.List;

public class InOrderTraversal implements Traversal{
	List<Node> in = new ArrayList<Node>(); 

	@Override
	public List<Node> traverse(Node n) {
		if(n != null){
			if(n.getLeftnode()!=null)traverse(n.getLeftnode());
			in.add(n);
			if(n.getrightnode()!=null)traverse(n.getrightnode());
			
		}
		return in;
	}

}
