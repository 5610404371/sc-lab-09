package TreeTraversal;

import java.util.ArrayList;

public class ReportConsole {
	public static void display(Node n,Traversal t){
		ArrayList<Node> ans = (ArrayList<Node>) t.traverse(n);
		
		for(int i =0; i<ans.size();i++){
			System.out.print(ans.get(i).getName()+" ");
		}
	}



	public static void main(String[] args) {
		Node c = new Node("C", null, null);
		Node e = new Node("E", null, null);
		Node d = new Node("D", c, e);
		Node a = new Node("A", null, null);
		Node b = new Node("B", a, d);
		Node h = new Node("H", null, null);
		Node i = new Node("I", h, null);
		Node g = new Node("G", i, null);
		Node f = new Node("F", b, g);
		
		System.out.print("Traversal with PreOrderTraversal: ");
		display(f,new PreOrderTraversal());
		System.out.println();
		System.out.print("Traversal with InOrderTraversal: ");
		display(f,new InOrderTraversal());
		System.out.println();
		System.out.print("Traversal with PostOrderTraversal: ");
		display(f,new PostOrderTraversal());
	}

}
