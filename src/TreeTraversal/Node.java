package TreeTraversal;

public class Node {
	String name;
	Node left;
	Node right;
	
	public Node(String name,Node left,Node right){
		this.name = name;
		this.left = left;
		this.right = right;
	}
	
	public String getName(){
		return name;
	}
	
	public Node getLeftnode(){
		return left;
	}
	
	public Node getrightnode(){
		return right;
	}

}
