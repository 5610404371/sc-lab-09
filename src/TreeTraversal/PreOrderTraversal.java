package TreeTraversal;

import java.util.ArrayList;
import java.util.List;


public class PreOrderTraversal implements Traversal{
	List<Node> pre = new ArrayList<Node>(); 

	@Override
	public List<Node> traverse(Node n) {
		if(n!=null){
			pre.add(n);
			if(n.getLeftnode()!=null)traverse(n.getLeftnode());
			if(n.getrightnode()!=null)traverse(n.getrightnode());
		}
		return pre;
	}


}
