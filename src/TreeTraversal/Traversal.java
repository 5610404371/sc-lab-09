package TreeTraversal;

import java.util.List;

public interface Traversal {
	public List<Node> traverse(Node n);

}
