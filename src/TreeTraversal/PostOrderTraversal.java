package TreeTraversal;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal{
	List<Node> post = new ArrayList<Node>(); 
	
	@Override
	public List<Node> traverse(Node n) {
		if(n != null){
			if(n.getLeftnode()!=null)traverse(n.getLeftnode());
			if(n.getrightnode()!=null)traverse(n.getrightnode());
			post.add(n);
		}
		return post;
		
	}

}
