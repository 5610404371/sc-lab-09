package comparator;

import Tax.Taxable;

public class Company implements Taxable{
	private String name;
	private int earning;
	private int expense;
	private int profit;
	
	public Company(String name,int earning,int expense){
		this.name = name;
		this.earning = earning;
		this.expense = expense;
		profit = earning-expense;
	}
	
	public int getEarning(){
		return earning;
	}
	
	public int getExpense(){
		return expense;
	}
	
	public int getProfit(){
		return profit;
	}
	
	@Override
	public double getTax() {
		double tax;
		tax = ((earning - expense)*30)/100;
		return tax;
	}
	
	public String toString(){
		return "Name : "+this.name+", earning = "+this.earning+", expense = "+this.expense
				+", profit = "+this.profit;
	}
	

}
