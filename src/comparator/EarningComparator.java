package comparator;

import java.util.Comparator;

public class EarningComparator implements Comparator{

	@Override
	public int compare(Object obj1, Object obj2) {
		int earning1 = ((Company)obj1).getEarning();
		int earning2 = ((Company)obj2).getEarning();
		if(earning1 < earning2){return -1;}
		if(earning1 > earning2){return 1;}
		return 0;
	}

}
