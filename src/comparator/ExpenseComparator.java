package comparator;

import java.util.Comparator;

public class ExpenseComparator implements Comparator{

	@Override
	public int compare(Object o1, Object o2) {
		int expense1 = ((Company)o1).getExpense();
		int expense2 = ((Company)o2).getExpense();
		if(expense1 < expense2){return -1;}
		if(expense1 > expense2){return 1;}
		return 0;
	}

}
