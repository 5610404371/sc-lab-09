package comparator;

import java.util.Comparator;

public class ProfitComparator implements Comparator{

	@Override
	public int compare(Object o1, Object o2) {
		int profit1 = ((Company)o1).getProfit();
		int profit2 = ((Company)o1).getProfit();
		if(profit1 < profit2){return -1;}
		if(profit1 > profit2){return 1;}
		return 0;
	}

}
