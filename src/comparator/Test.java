package comparator;

import java.util.ArrayList;
import java.util.Collections;

public class Test {
	
	public static void main(String[] args) {
	ArrayList<Company> company = new ArrayList<Company>();
	company.add(new Company("Wat",20000,4500));
	company.add(new Company("Bonus",35000,10500));
	company.add(new Company("Tar",29000,2000));
	
	Collections.sort(company, new EarningComparator());
	System.out.println("--------Sort with earning--------");
	for(Company e : company){
		System.out.println(e);
	}
	
	Collections.sort(company, new ExpenseComparator());
	System.out.println("--------Sort with expense--------");
	for(Company ex : company){
		System.out.println(ex);
	}
	
	Collections.sort(company, new ProfitComparator());
	System.out.println("--------Sort with profit--------");
	for(Company p : company){
		System.out.println(p);
	}
	
	}
}
